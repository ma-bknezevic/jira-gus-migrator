module.exports = class {
    static GUSWorkRecordTypes = {
        BUG: 'Bug',
        INVESTIGATION: 'Investigation',
        TEMPLATE: 'Template',
        TODO: 'ToDo',
        USER_STORY: 'User Story'
    };

    static GUSStatuses = {
        NEW: 'New',
        IN_PROGRESS: 'In Progress',
        READY_FOR_REVIEW: 'Ready for Review',
        INTEGRATE: 'Integrate',
        WAITING: 'Waiting',
        QA_IN_PROGRESS: 'QA In Progress',
        FIXED: 'Fixed',
        NOT_A_BUG: 'Not a Bug'
    };

    static GUSTaskStatuses = {
        NOT_STARTED: 'Not Started',
        IN_PROGRESS: 'In Progress',
        COMPLETED: 'Completed',
        WAITING_ON_SOMEONE_ELSE: 'Waiting on someone else',
        DEFERRED: 'Deferred'
    };

    static GUSFrequencies = {
        ALWAYS: 'Always',
        OFTEN: 'Often',
        SOMETIMES: 'Sometimes',
        RARELY: 'Rarely'
    };

    static GUSImpacts = {
        MALFUNCTIONING: 'Malfunctioning',
        HAS_WORKAROUND: 'Has Workaround'
    };
}
