const { Connection } = require('jsforce');
const https = require('https');

module.exports = class GUSClient {
    recordTypeIdCache = {};
    frequencyIdCache = {};
    impactIdCache = {};
    themeIdCache = {};
    userIdCache = {};
    productTagIdCache = {};
    buildIdCache = {};

    constructor({ username, password, instance = 'https://login.salesforce.com', version = '48.0' }) {
        this.username = username;
        this.password = password;
        this.instance = instance;
        this.version = version;
    }

    static SObjects = {
        EPIC: 'ADM_Epic__c',
        THEME: 'ADM_Theme__c',
        THEME_ASSIGNMENT: 'ADM_Theme_Assignment__c',
        WORK: 'ADM_Work__c',
        FREQUENCY: 'ADM_Frequency__c',
        IMPACT: 'ADM_Impact__c',
        RELATED_WORK: 'ADM_Parent_Work__c',
        USER: 'User',
        TASK: 'ADM_Task__c',
        FEED_ITEM: 'FeedItem',
        BUILD: 'ADM_Build__c'
    };

    login() {
        return new Promise((resolve, reject) => {
            const headers = {
                'User-Agent'      : 'node-client',
                'Accept'          : 'text/html,application/xhtml+xml,application/xml',
                'Accept-Encoding' : 'none',
                'Accept-Charset'  : 'utf-8',
                'Connection'      : 'close',
                'Content-Type'    : 'text/xml; charset=utf-8',
                'SOAPAction'      : '"urn:enterprise.soap.sforce.com/login"'
            };

            const body = `
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="urn:enterprise.soap.sforce.com" xmlns:fns="urn:fault.enterprise.soap.sforce.com" xmlns:ens="urn:sobject.enterprise.soap.sforce.com">
                <soap:Header></soap:Header>
                <soap:Body>
                    <tns:login>
                        <username>${this.username}</username>
                        <password>${this.password}</password>
                    </tns:login>
                </soap:Body>
            </soap:Envelope>
            `;

            const options = {
                host: this.instance,
                path: '/services/Soap/c/v48.0',
                method: 'POST',
                headers
            };

            const req = https.request(options, (res) => {
                let data = '';

                res.on('data', function(chunk) {
                    data += chunk;
                });

                res.on('end', () => {
                    try {
                        const sessionId = data.match(/<sessionId>(.*)<\/sessionId>/)[1];
                        this.conn = new Connection({
                            sessionId,
                            version: this.version,
                            serverUrl: `https://${this.instance}`
                        });
                        resolve();
                    } catch (ex) {
                        const exceptionMessage = data.match(/<sf:exceptionMessage>(.*)<\/sf:exceptionMessage>/)[1];
                        reject(new Error(`Something went wrong while authenticating to the target GUS org. (${exceptionMessage})`));
                    }
                });
            });

            req.write(body);
            req.end();
            req.on('error', e => reject(e));
        });
    }

    async query(soql) {
        return new Promise((resolve, reject) => {
            const records = [];
            const query = this.conn.query(soql)
                .on('record', function(record) {
                    records.push(record);
                })
                .on('end', function() {
                    resolve(records);
                })
                .on('error', function(err) {
                    console.error(err);
                    reject(err);
                })
                .run(({ autoFetch: true, maxFetch: 4000 }));
        });
    }

    async create(sobject, recordsForInsert) {
        return await this.conn.sobject(sobject).create(recordsForInsert);
    }

    async buildGUSEpicMapByProjectPrefix(prefix) {
        const epics = await this.getSFMEpicsByProjectPrefix(prefix);
        const epicMap = new Map();
        epics.forEach(epic => epicMap.set(epic.External_Id__c, epic.Id));
        return epicMap;
    }

    async getSFMEpicsByProjectPrefix(prefix) {
        return await this.query(`SELECT Id, External_ID__c FROM ${GUSClient.SObjects.EPIC} WHERE Name LIKE '${prefix}:%'`)
    }

    async getRecordTypeId(sobject, recordType) {
        if (!this.recordTypeIdCache[sobject]) {
            this.recordTypeIdCache[sobject] = {};
        }

        if (!this.recordTypeIdCache[sobject][recordType]) {
            const result = await this.query(`SELECT Id FROM RecordType WHERE SObjectType='${sobject}' AND Name = '${recordType}'`);
            this.recordTypeIdCache[sobject][recordType] = result[0].Id;
        }

        return this.recordTypeIdCache[sobject][recordType];
    }

    async createRecords(sobject, records) {
        return await this.conn.create(sobject, records);
    }

    async getFrequencyIdByName(frequency) {
        if (!this.frequencyIdCache.hasOwnProperty(frequency)) {
            const result = await this.query(`SELECT Id FROM ${GUSClient.SObjects.FREQUENCY} WHERE Name = '${frequency}'`);
            this.frequencyIdCache[frequency] = result[0].Id;
        }

        return this.frequencyIdCache[frequency];
    }

    async getImpactIdByName(impact) {
        if (!this.impactIdCache.hasOwnProperty(impact)) {
            const result = await this.query(`SELECT Id FROM ${GUSClient.SObjects.IMPACT} WHERE Name = '${impact}'`);
            this.impactIdCache[impact] = result[0].Id;
        }

        return this.impactIdCache[impact];
    }

    async getThemeIdByName(theme) {
        if (!this.themeIdCache.hasOwnProperty(theme)) {
            let result = await this.query(`SELECT Id FROM ${GUSClient.SObjects.THEME} WHERE Name = '${theme}'`);

            // theme does not exist so create it
            if (!result.length) {
                result = await this.createRecords(GUSClient.SObjects.THEME, [{
                    Name: theme,
                    Active__c: true,
                    Description__c: 'Generated during data migration.'
                }]);
            }

            this.themeIdCache[theme] = result[0].Id;
        }

        return this.themeIdCache[theme];
    }

    async getUserIdByEmail(email) {
        if (!email) return null;

        if (!this.userIdCache.hasOwnProperty(email)) {
            let result = await this.query(`SELECT Id FROM ${GUSClient.SObjects.USER} WHERE Email = '${email}'`);
            this.userIdCache[email] = (result.length) ? result[0].Id : null;
        }

        return this.userIdCache[email];
    }

    async getProductTagIdByName(name) {
        if (!this.productTagIdCache.hasOwnProperty(name)) {
            let result = await this.query(`SELECT Id FROM ADM_Product_Tag__c WHERE Name = '${name}'`);
            this.productTagIdCache[name] = (result.length) ? result[0].Id : null;
        }

        return this.productTagIdCache[name];
    }

    async getBuildIdByVersionName(version) {
        if (!this.buildIdCache.hasOwnProperty(version)) {
            let result = await this.query(`SELECT Id FROM ADM_Build__c WHERE Name = '${version}'`);

            // build record does not exist so create it
            if (!result.length) {
                result = await this.createRecords(GUSClient.SObjects.BUILD, [{ Name: version }]);

                if (!result[0].success) {
                    throw new Error(`Could not create version due to errors: ${result[0].errors}`);
                }
            }

            this.buildIdCache[version] = result[0].Id;
        }

        return this.buildIdCache[version];
    }
}
