const JiraAPI = require('jira-client');

module.exports = class {
    constructor({ host, username, password, apiVersion = '2' }) {
        this.conn = new JiraAPI({
            protocol: 'https',
            host,
            username,
            password,
            apiVersion,
            strictSSL: true
        });
    }

    jiraSearchOptions = {
        startAt: 0,
        maxResults: 50,
        fields: ['*all'],
        expand: ['renderedFields', 'renderedBody']
    };

    async* issueRetriever(query, options = { startAt: 0, maxResults: 100 }) {
        let total = 0;
        let fetch = true;
        const _options = { ...options, ...this.jiraSearchOptions };

        while (fetch) {
            const response = await this.conn.searchJira(query, _options);
            total = response.total || 0;
            _options.startAt += response.issues.length;
            yield response.issues;

            if (_options.startAt === total) {
                fetch = false;
                _options.startAt = 0;
            }
        }
    }
};
