const GUSClient = require('./GUSClient');
const GUSEnums = require('./GUS.enums');

module.exports = class {
    constructor(config, GUSConn, JiraConn) {
        this.config = config;
        this.GUSConn = GUSConn;
        this.JiraConn = JiraConn;
    }

    async createThemeAssignments(themeAssignmentMap, issueKeyToWorkIdMap) {
        // populate work lookup on theme assignments
        const themeAssignmentsForInsert = [];
        for (const [issueKey, themeAssignment] of themeAssignmentMap) {
            const workId = issueKeyToWorkIdMap.get(issueKey);
            if (workId) {
                themeAssignmentsForInsert.push({
                    ...themeAssignment,
                    ...{
                        Work__c: workId
                    }
                })
            }
        }

        // create theme assignments
        if (themeAssignmentsForInsert.length) {
            await this.createRecordsHelper(GUSClient.SObjects.THEME_ASSIGNMENT, themeAssignmentsForInsert);
        }
    }

    async migrateEpicsByQuery(query) {
        // get current SFM epics in GUS; used to make sure we do not duplicate epics
        this.SFMEpicMap = await this.GUSConn.buildGUSEpicMapByProjectPrefix(this.config.name);

        for await (const epics of this.JiraConn.issueRetriever(query)) {
            const epicsForInsert = [];
            console.log('num epics', epics.length);

            for await (let epic of epics) {
                // epic already exists; do not duplicate in GUS
                if (this.SFMEpicMap.has(epic.key)) continue;

                epicsForInsert.push({
                    Name: `${this.config.name}: ${epic.fields.summary}`.substring(0, 80),
                    External_ID__c: epic.key,
                    Description__c: `${epic.fields.summary}\r\n\r\n${epic.fields.description}` // not a rich text field in GUS
                });
            }

            if (epicsForInsert.length) {
                // create epics
                const results = await this.createRecordsHelper(GUSClient.SObjects.EPIC, epicsForInsert);

                // Add new epics to map
                epicsForInsert.forEach((epic, index) => this.SFMEpicMap.set(epic.External_ID__c, results[index].id));
            }
        }
    }

    async createRelatedWork(relatedWorkMap, issueKeyToWorkIdMap) {
        let relatedWorkForInsert = [];
        
        for (const [parentIssueKey, childIssueKeys] of relatedWorkMap) {
            if (!issueKeyToWorkIdMap.has(parentIssueKey)) continue;

            for (let i = 0; i < childIssueKeys.length; i++) {
                const childIssueKey = childIssueKeys[i];
                if (!issueKeyToWorkIdMap.has(childIssueKey)) continue;

                relatedWorkForInsert.push({
                    Parent_Work__c: issueKeyToWorkIdMap.get(parentIssueKey),
                    Child_Work__c: issueKeyToWorkIdMap.get(childIssueKey)
                });

                if (relatedWorkForInsert.length === 200) {
                    await this.createRecordsHelper(GUSClient.SObjects.RELATED_WORK, relatedWorkForInsert);
                    relatedWorkForInsert = [];
                }
            }
        }

        if (relatedWorkForInsert.length) {
            await this.createRecordsHelper(GUSClient.SObjects.RELATED_WORK, relatedWorkForInsert);
        }
    }

    async createRecordsHelper(sobject, recordsForInsert) {
        const results = await this.GUSConn.createRecords(sobject, recordsForInsert);
        results.forEach(({ success, errors }, index) => {
            if (!success) {
                console.error(recordsForInsert[index]);
                console.error(errors);
                throw new Error('Could not insert records in GUS!');
            }
        });

        console.log(`Successfully inserted ${recordsForInsert.length} ${sobject.toString()} record(s) in GUS.`);
        return results;
    }

    async createTasks(tasksForInsertMap, issueKeyToWorkIdMap) {
        const tasksForInsert = [];
        
        for (const [parentIssueKey, tasks] of tasksForInsertMap) {
            if (!issueKeyToWorkIdMap.has(parentIssueKey)) continue;

            tasks.forEach((task) => {
                tasksForInsert.push({
                    ...task,
                    ...{
                        Work__c: issueKeyToWorkIdMap.get(parentIssueKey)
                    }
                })
            });
        }

        await this.createRecordsHelper(GUSClient.SObjects.TASK, tasksForInsert);
    }

    addGUSTaskToMap(issue, tasksForInsertMap) {
        if (!tasksForInsertMap.has(issue.fields.parent.key)) {
            tasksForInsertMap.set(issue.fields.parent.key, []);
        }

        tasksForInsertMap.get(issue.fields.parent.key).push({
            Assigned_To__c: '0052g000000OoVOAA0', // TODO
            Subject__c: issue.fields.summary,
            Status__c: this.config.mappings.taskStatus(issue),
            External_ID__c: issue.key
        });
    }

    async createFeedItems(commentsForInsertMap, issueKeyToWorkIdMap) {
        let feedItemsForInsert = [];
        
        for (const [parentIssueKey, feedItems] of commentsForInsertMap) {
            if (!issueKeyToWorkIdMap.has(parentIssueKey)) continue;

            for (let i = feedItems.length - 1; i >= 0; i--) {
                const feedItem = feedItems[i];

                feedItemsForInsert.push({
                    ...feedItem,
                    ...{
                        ParentId: issueKeyToWorkIdMap.get(parentIssueKey)
                    }
                });

                if (feedItemsForInsert.length === 200) {
                    await this.createRecordsHelper(GUSClient.SObjects.FEED_ITEM, feedItemsForInsert);
                    feedItemsForInsert = [];
                }
            }
        }

        if (feedItemsForInsert.length) {
            await this.createRecordsHelper(GUSClient.SObjects.FEED_ITEM, feedItemsForInsert);
        }
    }

    stripAnchorAttributes(str) {
        let match;

        while (match = /<a href="[^"]+[^>]+rel=[^>]+>(.*?)<\/a>/gm.exec(str)) {
            str = str.substring(0, match.index) + `<b>@${match[1]}</b>` + str.substring(match.index + match[0].length);
        }

        return str;
    }

    stripUnsupportedElements(str) {
        let match;

        while (match = /<del[^>]*|<ins>|<span[^>]*>|<em[^>]*>|<sup.+?<\/sup>/gm.exec(str)) {
            str = str.substring(0, match.index) + str.substring(match.index + match[0].length);
        }

        return str.replace(/<\/del>|<\/ins>|<\/span>|<\/em>/gm, '').replace(/<tt>/gm, '<i>').replace(/<\/tt>/gm, '</i>');
    }

    convertImagesToAnchors(str) {
        let match;

        while (match = /<a.*?href="(.*?)".*?file-preview-type="image".*?<\/a>/gm.exec(str)) {``
            str = str.substring(0, match.index) + `<a href="https://mapanything.atlassian.net${match[1]}">Image Link</a>` + str.substring(match.index + match[0].length);
        }

        while (match = /<img[^>]+>/gm.exec(str)) {
            let anchor = match[0].replace('img', 'a').replace('src="', 'href="https://mapanything.atlassian.net').replace('/>', '>Image Link</a>');
            str = str.substring(0, match.index) + anchor + str.substring(match.index + match[0].length);
        }

        return str;
    }

    convertCodeBlocks(str) {
        let match;
        let foundCodeBlock;
    
        while (match = /<div.*?<pre.*?>|<blockquote>/gms.exec(str)) {
            foundCodeBlock = true;
            str = str.substring(0, match.index) + '<code>' + str.substring(match.index + match[0].length);
        }
    
        while (match = /<\/pre>\s*?<\/div>\s*?<\/div>|<\/blockquote>/gm.exec(str)) {
            str = str.substring(0, match.index) + '</code>' + str.substring(match.index + match[0].length);
        }

        if (foundCodeBlock) {
            // remove markup from code block
            const start = str.match(/<code>/).index + 6;
            const end = str.match(/<\/code>/).index;
            const stripped = str.substring(start, end).replace(/<.*?>/gm, '');
            str = str.substring(0, start) + stripped + str.substring(end);
        }
    
        return str.replace(/<br\s?\/>/gm, '\r');
    }

    removeEmbeddedObjects(str) {
        let match;

        while (match = /<div class="embeddedObject".*?<\/div>/gm.exec(str)) {
            str = str.substring(0, match.index) + '<p>&nbsp;</p><i>Unsupported Media Object Redacted During Migration</i>' + str.substring(match.index + match[0].length);
        }

        return str;
    }

    addLineBreaks(str) {
        return str.replace(/<\/p>\s*?<p>/gm, '</p><p>&nbsp;</p><p>');
    }

    cleanCommentBody(str) {
        str = this.stripAnchorAttributes(str);
        str = this.stripUnsupportedElements(str);
        str = this.convertImagesToAnchors(str);
        str = this.convertCodeBlocks(str);
        str = this.removeEmbeddedObjects(str);
        str = this.addLineBreaks(str);
        return str;
    }

    async buildFeedItemsForInsert(comments) {
        const feedItemsForInsert = [];

        for await (const comment of comments) {
            feedItemsForInsert.push({
                CreatedById: await this.GUSConn.getUserIdByEmail(comment.author.emailAddress),
                Body: this.cleanCommentBody(comment.body),
                isRichText: true
            });
        }

        return feedItemsForInsert;
    }

    async buildWorkRecord(issue) {
        const recordTypeName = this.config.mappings.issueType(issue.fields.issuetype.name);
        const productTag = (typeof this.config.productTag === 'function') ? this.config.productTag(issue) : this.config.productTag;

        let work = {
            CreatedById: await this.GUSConn.getUserIdByEmail((issue.fields.reporter) ? issue.fields.reporter.emailAddress : issue.fields.creator.emailAddress),
            Subject__c: issue.fields.summary,
            Product_Tag__c: await this.GUSConn.getProductTagIdByName(productTag),
            Status__c: this.config.mappings.issueStatus(issue),
            External_Id__c: issue.key,
            RecordTypeId: await this.GUSConn.getRecordTypeId(GUSClient.SObjects.WORK, recordTypeName)
        };

        if (issue.fields.assignee) {
            work.Assignee__c = await this.GUSConn.getUserIdByEmail(issue.fields.assignee.emailAddress);
        }

        switch (recordTypeName) {
            // bug
            case GUSEnums.GUSWorkRecordTypes.BUG:
                const GUSPriority = this.config.mappings.issuePriority(issue.fields.priority.name);
                let version = (issue.fields.fixVersions.length) ? issue.fields.fixVersions[0].name : this.config.defaultVersion;

                if (this.config.versionPrefix) {
                    version = this.config.versionPrefix + version;
                }

                work = {
                    ...work,
                    ...{
                        Details_and_Steps_to_Reproduce__c: this.addLineBreaks(issue.renderedFields.description),
                        Found_in_Build__c: await this.GUSConn.getBuildIdByVersionName(version),
                        Frequency__c: await this.GUSConn.getFrequencyIdByName(GUSPriority.frequency),
                        Impact__c: await this.GUSConn.getImpactIdByName(GUSPriority.impact)
                    }
                };
                break;

            // user story
            case GUSEnums.GUSWorkRecordTypes.USER_STORY:
                work.Details__c = this.addLineBreaks(issue.renderedFields.description);
                work.Epic__c = this.SFMEpicMap.get(this.config.getEpicKey(issue));
                break;

            default:
        }

        return work;
    }
}
