const GUSEnums = require('../GUS.enums');

const JiraPriorities = {
    HIGHEST: 'Highest',
    HIGH: 'High',
    MEDIUM: 'Medium',
    LOW: 'Low',
    LOWEST: 'Lowest'
};

const JiraStatuses = {
    TO_DO: 'ToDo',
    QA_QC: 'QA/QC',
    IN_PROGRESS: 'In Progress'
};

const JiraIssueTypes = {
    STORY: 'Story',
    BUG: 'Bug',
    TASK: 'Task',
    SUB_TASK: 'Sub-task'
};

module.exports = {
    name: 'MADE',
    epicQuery: 'project = MADE AND type = Epic AND status in ("In Progress", "QA/QC", "ToDo")',
    issueQuery: 'project = MADE AND type != Epic AND status in ("In Progress", "QA/QC", "ToDo")',
    // if a bug does not have a fixed-in-build value then we will use the version in GUS specified below (will be created if it does not exist)
    defaultVersion: 'MADE Placeholder',
    versionPrefix: 'FWE ',
    productTag: 'OEng Developer Tools',
    // TODO: Jira tasks are directly linked to an epic which makes them orphans in GUS
    // Possible solution is to create a "Technical Tasks" Work record to park all of these orphaned sub-tasks.
    skipIssue: issue => issue.fields.issuetype.name === JiraIssueTypes.TASK,
    // if the issue is a sub-task then create a GUS task record and continue
    convertToGUSTask: issue => issue.fields.issuetype.name === JiraIssueTypes.SUB_TASK,
    getEpicKey: (issue) => {
        if (issue.fields.parent && issue.fields.parent.fields.issuetype.name === 'Epic') {
            return issue.fields.parent.key;
        }
    },
    mappings: {
        formatEpicName: epic => `MADE: ${epic.fields.summary}`,
        issueStatus: (issue) => {
            const status = issue.fields.status.name;

            switch (status) {
                case JiraStatuses.TO_DO:
                    return GUSEnums.GUSStatuses.NEW;

                case JiraStatuses.IN_PROGRESS:
                    return GUSEnums.GUSStatuses.IN_PROGRESS;

                case JiraStatuses.QA_QC:
                    return GUSEnums.GUSStatuses.QA_IN_PROGRESS;

                default:
                    throw new Error(`Could not map Jira status: ${status}`);
            }
        },
        issuePriority: (priority) => {
            const impact = (priority === JiraPriorities.HIGHEST || JiraPriorities.HIGH)
                ? GUSEnums.GUSImpacts.MALFUNCTIONING
                : GUSEnums.GUSImpacts.HAS_WORKAROUND;

            return {
                frequency: GUSEnums.GUSFrequencies.RARELY,
                impact
            };
        },
        issueType: (type) => {
            switch (type) {
                case JiraIssueTypes.STORY:
                    return GUSEnums.GUSWorkRecordTypes.USER_STORY;

                case JiraIssueTypes.BUG:
                    return GUSEnums.GUSWorkRecordTypes.BUG;

                default:
                    throw new Error(`Could not map Jira issue type: ${type}`);
            }
        }
    }
}
