const GUSEnums = require('../GUS.enums');

const JiraPriorities = {
    HIGHEST: 'Highest',
    HIGH: 'High',
    MEDIUM: 'Medium',
    LOW: 'Low',
    LOWEST: 'Lowest'
};

const JiraStatuses = {
    BACKLOG: 'Backlog',
    MR_CREATED: 'MR-Created',
    TO_DO: 'To Do',
    BLOCKED: 'Blocked',
    DEV_ENV: 'Dev Env',
    IN_PROGRESS: 'In Progress',
    MR_ACCEPTED: 'MR-Accepted',
    REJECTED_BY_QA: 'Rejected by QA',
    STAGING_ENV: 'Staging Env',
    DONE: 'Done',
    NOT_DOING: 'Not Doing'    
};

const JiraIssueTypes = {
    STORY: 'Story',
    BUG: 'Bug',
    TASK: 'Task',
    SUB_TASK: 'Sub-task'
};

module.exports = {
    name: 'MAPI',
    epicQuery: 'project = MAPI AND type = Epic AND status in("To Do","In Progress", "Blocked")',
    issueQuery: 'project = MAPI AND type != Epic AND status in("To Do","In Progress", "Blocked", "mr-created", "mr-accepted")',
    // if a bug does not have a fixed-in-build value then we will use the version in GUS specified below (will be created if it does not exist)
    defaultVersion: 'SFMaps Off-Platform 1.0',
    productTag: 'SFMaps-MAIO',
    // TODO: Jira tasks are directly linked to an epic which makes them orphans in GUS
    // Possible solution is to create a "Technical Tasks" Work record to park all of these orphaned sub-tasks.
    skipIssue: issue => issue.fields.issuetype.name === JiraIssueTypes.TASK,
    // if the issue is a sub-task then create a GUS task record and continue
    convertToGUSTask: issue => issue.fields.issuetype.name === JiraIssueTypes.SUB_TASK,
    getEpicKey: epic => epic.fields.customfield_10017,
    mappings: {
        issueStatus: (issue) => {
            const status = issue.fields.status.name;
            const type = issue.fields.issuetype.name

            switch (status) {
                case JiraStatuses.TO_DO:
                    return GUSEnums.GUSStatuses.NEW;

                case JiraStatuses.IN_PROGRESS:
                    return GUSEnums.GUSStatuses.IN_PROGRESS;

                case JiraStatuses.MR_CREATED:
                    return GUSEnums.GUSStatuses.READY_FOR_REVIEW;

                case JiraStatuses.MR_ACCEPTED:
                    return GUSEnums.GUSStatuses.INTEGRATE;

                case JiraStatuses.BLOCKED:
                    return GUSEnums.GUSStatuses.WAITING;
            
                case JiraStatuses.DEV_ENV:
                    return GUSEnums.GUSStatuses.QA_IN_PROGRESS;

                case JiraStatuses.DONE:
                    return GUSEnums.GUSStatuses.FIXED;

                case JiraStatuses.NOT_DOING:
                    switch (type) {
                        case JiraIssueTypes.BUG:
                            return GUSEnums.GUSStatuses.NOT_A_BUG;
                    
                        case JiraIssueTypes.STORY:
                            return GUSEnums.GUSStatuses.DEFERRED;

                        default:
                            throw new Error(`Could not map Jira status 'Not Doing' for Jira issue type: ${type}`);
                    }

                default:
                    throw new Error(`Could not map Jira status: ${status}`);
            }
        },
        taskStatus: (issue) => {
            const status = issue.fields.status.name;

            switch (status) {
                case JiraStatuses.TO_DO:
                    return GUSEnums.GUSTaskStatuses.NOT_STARTED;

                case JiraStatuses.IN_PROGRESS:
                case JiraStatuses.MR_REJECTED:
                case JiraStatuses.READY_FOR_QA:
                case JiraStatuses.REJECTED_BY_QA:
                    return GUSEnums.GUSTaskStatuses.IN_PROGRESS;

                case JiraStatuses.MR_CREATED:
                case JiraStatuses.MR_ACCEPTED:
                case JiraStatuses.MR_APPROVED:
                case JiraStatuses.BLOCKED:
                case JiraStatuses.BLOCKED_MAIO:
                    return GUSEnums.GUSTaskStatuses.WAITING_ON_SOMEONE_ELSE;

                case JiraStatuses.DONE:
                    return GUSEnums.GUSTaskStatuses.COMPLETED;

                case JiraStatuses.NOT_DOING:
                    return GUSEnums.GUSTaskStatuses.DEFERRED;

                default:
                    throw new Error(`Could not map Jira status: ${status}`);
            }
        },
        issuePriority: (priority) => {
            const impact = (priority === JiraPriorities.HIGHEST || JiraPriorities.HIGH)
                ? GUSEnums.GUSImpacts.MALFUNCTIONING
                : GUSEnums.GUSImpacts.HAS_WORKAROUND;

            return {
                frequency: GUSEnums.GUSFrequencies.RARELY,
                impact
            };
        },
        issueType: (type) => {
            switch (type) {
                case JiraIssueTypes.STORY:
                    return GUSEnums.GUSWorkRecordTypes.USER_STORY;

                case JiraIssueTypes.BUG:
                    return GUSEnums.GUSWorkRecordTypes.BUG;

                default:
                    throw new Error(`Could not map Jira issue type: ${type}`);
            }
        }
    }
}
