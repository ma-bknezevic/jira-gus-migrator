const GUSEnums = require('../GUS.enums');

const JiraStatuses = {
    TO_DO: 'To Do',
    READY_FOR_DEV: 'Ready for Dev',
    IN_PROGRESS: 'In Progress',
    READY_FOR_QA_TESTING: 'Ready for QA Testing',
    TO_BE_DEPLOYED: 'To Be Deployed',
    DONE: 'Done'
};

module.exports = {
    name: 'MAQAR',
    issueQuery: 'project = MAQAR',
    productTag: 'SFMaps-MAIO',
    mappings: {
        issueStatus: () => GUSEnums.GUSStatuses.INTEGRATE,
        issueType: () => GUSEnums.GUSWorkRecordTypes.USER_STORY
    }
}
