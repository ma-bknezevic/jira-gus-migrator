const GUSEnums = require('../GUS.enums');

const JiraPriorities = {
    HIGHEST: 'Highest',
    HIGH: 'High',
    MEDIUM: 'Medium',
    LOW: 'Low',
    LOWEST: 'Lowest'
};

const JiraStatuses = {
    TO_DO: 'To Do',
    READY_FOR_DEV: 'Ready for Dev',
    IN_PROGRESS: 'In Progress',
    READY_FOR_QA_TESTING: 'Ready for QA Testing',
    TO_BE_DEPLOYED: 'To Be Deployed',
};

const JiraIssueTypes = {
    STORY: 'Story',
    BUG: 'Bug',
    TASK: 'Task',
    SUB_TASK: 'Sub-task'
};

module.exports = {
    name: 'MARE',
    epicQuery: 'project = MARE AND type = Epic AND status in ("In Progress", "Ready For Dev", "Ready for QA Testing", "To Be Deployed", "To Do")',
    issueQuery: 'project = MARE AND type != Epic AND status in ("In Progress", "Ready For Dev", "Ready for QA Testing", "To Be Deployed", "To Do")',
    // if a bug does not have a fixed-in-build value then we will use the version in GUS specified below (will be created if it does not exist)
    defaultVersion: 'MARE Placeholder',
    versionPrefix: 'FWE ',
    productTag: 'O2 Optimization Engine',
    // TODO: Jira tasks are directly linked to an epic which makes them orphans in GUS
    // Possible solution is to create a "Technical Tasks" Work record to park all of these orphaned sub-tasks.
    skipIssue: issue => issue.fields.issuetype.name === JiraIssueTypes.TASK,
    // if the issue is a sub-task then create a GUS task record and continue
    convertToGUSTask: issue => issue.fields.issuetype.name === JiraIssueTypes.SUB_TASK,
    getEpicKey: epic => epic.fields.customfield_10017,
    mappings: {
        issueStatus: (issue) => {
            const status = issue.fields.status.name;
            
            switch (status) {
                case JiraStatuses.TO_DO:
                case JiraStatuses.READY_FOR_DEV:
                    return GUSEnums.GUSStatuses.NEW;

                case JiraStatuses.IN_PROGRESS:
                    return GUSEnums.GUSStatuses.IN_PROGRESS;

                case JiraStatuses.READY_FOR_QA_TESTING:
                    return GUSEnums.GUSStatuses.READY_FOR_REVIEW;

                case JiraStatuses.TO_BE_DEPLOYED:
                    return GUSEnums.GUSStatuses.INTEGRATE;

                case JiraStatuses.DONE:
                    return GUSEnums.GUSStatuses.FIXED;

                case JiraStatuses.NOT_DOING:
                    return GUSEnums.GUSStatuses.NOT_A_BUG;    

                default:
                    throw new Error(`Could not map Jira status: ${status}`);
            }
        },
        issuePriority: (priority) => {
            const impact = (priority === JiraPriorities.HIGHEST || JiraPriorities.HIGH)
                ? GUSEnums.GUSImpacts.MALFUNCTIONING
                : GUSEnums.GUSImpacts.HAS_WORKAROUND;

            return {
                frequency: GUSEnums.GUSFrequencies.RARELY,
                impact
            };
        },
        issueType: (type) => {
            switch (type) {
                case JiraIssueTypes.STORY:
                    return GUSEnums.GUSWorkRecordTypes.USER_STORY;

                case JiraIssueTypes.BUG:
                    return GUSEnums.GUSWorkRecordTypes.BUG;

                default:
                    throw new Error(`Could not map Jira issue type: ${type}`);
            }
        }
    }
}
