const GUSEnums = require('../GUS.enums');

const JiraPriorities = {
    HIGHEST: 'Highest',
    HIGH: 'High',
    MEDIUM: 'Medium',
    LOW: 'Low',
    LOWEST: 'Lowest'
};

const JiraStatuses = {
    TO_DO: 'To Do',
    IN_PROGRESS: 'In Progress',
    MR_CREATED: 'MR Created',
    MR_REJECTED: 'MR Rejected',
    MR_ACCEPTED: 'MR Accepted',
    MR_APPROVED: 'MR Approved',
    BLOCKED: 'Blocked',
    BLOCKED_MAIO: 'Blocked (MAIO)',
    READY_FOR_QA: 'Ready for QA',
    REJECTED_BY_QA: 'Rejected by QA',
    DONE: 'Done',
    NOT_DOING: 'Not Doing'
};

const JiraIssueTypes = {
    STORY: 'Story',
    BUG: 'Bug',
    TASK: 'Task',
    SUB_TASK: 'Sub-task'
};

module.exports = {
    name: 'SFMM',
    epicQuery: 'project = SFMM AND type = Epic',
    issueQuery: 'project = SFMM AND type != Epic AND status NOT IN (Done, "Not Doing")',
    // if a bug does not have a fixed-in-build value then we will use the version in GUS specified below (will be created if it does not exist)
    defaultVersion: 'SFMaps 2.14.2',
    productTag: 'Maps - Managed Package',
    // productTag: (issue) => {
    //     const componentName = (issue.fields.components.length) ? issue.fields.components[0].name : 'Maps';
    //     return `${issue.fields.project.key} - ${componentName}`;
    // },
    // TODO: Jira tasks are directly linked to an epic which makes them orphans in GUS
    // Possible solution is to create a "Technical Tasks" Work record to park all of these orphaned sub-tasks.
    skipIssue: issue => issue.fields.issuetype.name === JiraIssueTypes.TASK,
    // if the issue is a sub-task then create a GUS task record and continue
    convertToGUSTask: issue => issue.fields.issuetype.name === JiraIssueTypes.SUB_TASK,
    getEpicKey: epic => epic.fields.customfield_10017,
    mappings: {
        issueStatus: (issue) => {
            const status = issue.fields.status.name;

            switch (status) {
                case JiraStatuses.TO_DO:
                    return GUSEnums.GUSStatuses.NEW;

                case JiraStatuses.IN_PROGRESS:
                case JiraStatuses.MR_REJECTED:
                case JiraStatuses.REJECTED_BY_QA:
                    return GUSEnums.GUSStatuses.IN_PROGRESS;

                case JiraStatuses.MR_CREATED:
                    return GUSEnums.GUSStatuses.READY_FOR_REVIEW;

                case JiraStatuses.MR_ACCEPTED:
                case JiraStatuses.MR_APPROVED:
                    return GUSEnums.GUSStatuses.INTEGRATE;

                case JiraStatuses.BLOCKED:
                case JiraStatuses.BLOCKED_MAIO:
                    return GUSEnums.GUSStatuses.WAITING;

                case JiraStatuses.READY_FOR_QA:
                    return GUSEnums.GUSStatuses.QA_IN_PROGRESS;

                case JiraStatuses.DONE:
                    return GUSEnums.GUSStatuses.FIXED;

                case JiraStatuses.NOT_DOING:
                    return GUSEnums.GUSStatuses.NOT_A_BUG;

                default:
                    throw new Error(`Could not map Jira status: ${status}`);
            }
        },
        taskStatus: (issue) => {
            const status = issue.fields.status.name;

            switch (status) {
                case JiraStatuses.TO_DO:
                    return GUSEnums.GUSTaskStatuses.NOT_STARTED;

                case JiraStatuses.IN_PROGRESS:
                case JiraStatuses.MR_REJECTED:
                case JiraStatuses.READY_FOR_QA:
                case JiraStatuses.REJECTED_BY_QA:
                    return GUSEnums.GUSTaskStatuses.IN_PROGRESS;

                case JiraStatuses.MR_CREATED:
                case JiraStatuses.MR_ACCEPTED:
                case JiraStatuses.MR_APPROVED:
                case JiraStatuses.BLOCKED:
                case JiraStatuses.BLOCKED_MAIO:
                    return GUSEnums.GUSTaskStatuses.WAITING_ON_SOMEONE_ELSE;

                case JiraStatuses.DONE:
                    return GUSEnums.GUSTaskStatuses.COMPLETED;

                case JiraStatuses.NOT_DOING:
                    return GUSEnums.GUSTaskStatuses.DEFERRED;

                default:
                    throw new Error(`Could not map Jira status: ${status}`);
            }
        },
        issuePriority: (priority) => {
            const impact = (priority === JiraPriorities.HIGHEST || JiraPriorities.HIGH)
                ? GUSEnums.GUSImpacts.MALFUNCTIONING
                : GUSEnums.GUSImpacts.HAS_WORKAROUND;

            return {
                frequency: GUSEnums.GUSFrequencies.RARELY,
                impact
            };
        },
        issueType: (type) => {
            switch (type) {
                case JiraIssueTypes.STORY:
                    return GUSEnums.GUSWorkRecordTypes.USER_STORY;

                case JiraIssueTypes.BUG:
                    return GUSEnums.GUSWorkRecordTypes.BUG;

                default:
                    throw new Error(`Could not map Jira issue type: ${type}`);
            }
        }
    }
}
