const JiraClient = require('./JiraClient');
const GUSClient = require('./GUSClient');
const _MigrationHelper = require('./MigrationHelper');
const config = require(`./configs/${process.env.PROJECT}.config`);

const GUSVars = {
    instance: process.env.GUS_INSTANCE,
    username: process.env.GUS_USERNAME,
    password: process.env.GUS_PASSWORD,
    version: process.env.GUS_API_VERSION
};

const JiraVars = {
    host: process.env.JIRA_HOST,
    username: process.env.JIRA_USERNAME,
    password: process.env.JIRA_PASSWORD,
    apiVersion: process.env.JIRA_API_VERSION || '2' // version 3 is in beta; specifically has bugs related to renderedFields support
};

(async function() {
    try {
        // authenticate with GUS
        const GUSConn = new GUSClient(GUSVars)
        await GUSConn.login();

        // authenticate with Jira
        const JiraConn = await new JiraClient(JiraVars);
        const MigrationHelper = new _MigrationHelper(config, GUSConn, JiraConn);

        // migrate epics
        if (config.epicQuery) {
            await MigrationHelper.migrateEpicsByQuery(config.epicQuery);
        }

        const issueKeyToWorkIdMap = new Map();
        const tasksForInsertMap = new Map();
        const themeAssignmentMap = new Map();
        const relatedWorkMap = new Map(); // parent -> child[]
        const commentsForInsertMap = new Map();

        let numIssuesSkipped = 0;

        // fetch jira issues        
        for await (const issues of JiraConn.issueRetriever(config.issueQuery)) {
            console.log('num issues:', issues.length);
            const workForInsert = [];

            for await (let issue of issues) {
                // this issue is a sub-task; add child work record entry
                if (issue.fields.parent) {
                    if (!relatedWorkMap.has(issue.fields.parent.key)) {
                        relatedWorkMap.set(issue.fields.parent.key, []);
                    }

                    relatedWorkMap.get(issue.fields.parent.key).push(issue.key);
                }

                // this issue is related to another issue; add parent work record entry
                issue.fields.issuelinks.forEach((link) => {
                    if (!relatedWorkMap.has(issue.key)) {
                        relatedWorkMap.set(issue.key, []);
                    }

                    let linkedIssueKey;

                    if (link.inwardIssue) {
                        linkedIssueKey = link.inwardIssue.key;
                    } else if (link.outwardIssue) {
                        linkedIssueKey = link.outwardIssue.key;
                    }

                    relatedWorkMap.get(issue.key).push(linkedIssueKey);
                });

                if (typeof config.skipIssue === 'function' && config.skipIssue(issue)) {
                    numIssuesSkipped++;
                    continue;
                }

                if (typeof config.convertToGUSTask === 'function' && config.convertToGUSTask(issue)) {
                    MigrationHelper.addGUSTaskToMap(issue, tasksForInsertMap);
                    continue;
                }

                // build work record
                const work = await MigrationHelper.buildWorkRecord(issue);
                workForInsert.push(work);

                // process Jira labels and convert to theme assignments
                for (let i = 0; i < issue.fields.labels.length; i++) {
                    const label = issue.fields.labels[i];

                    themeAssignmentMap.set(issue.key, {
                        Theme__c: await GUSConn.getThemeIdByName(label)
                    });
                }

                const { comments } = issue.renderedFields.comment;

                if (comments.length) {
                    commentsForInsertMap.set(issue.key, await MigrationHelper.buildFeedItemsForInsert(comments));
                }
            }

            // create work records in GUS
            const results = await MigrationHelper.createRecordsHelper(GUSClient.SObjects.WORK, workForInsert);

            // add map entries: jiraIssueKey => workRecordId
            workForInsert.forEach((work, index) => issueKeyToWorkIdMap.set(work.External_ID__c, results[index].id));
        }

        // Use this to build the issueKeyToWorkIdMap data structure from existing work records if needed
        // const insertedWork = await GUSConn.query(`SELECT Id, External_ID__c FROM ADM_Work__c WHERE External_ID__c LIKE '${config.name}%'`);
        // insertedWork.forEach((work, index) => issueKeyToWorkIdMap.set(work.External_ID__c, work.Id));

        await MigrationHelper.createFeedItems(commentsForInsertMap, issueKeyToWorkIdMap);
        await MigrationHelper.createTasks(tasksForInsertMap, issueKeyToWorkIdMap);
        await MigrationHelper.createThemeAssignments(themeAssignmentMap, issueKeyToWorkIdMap);
        await MigrationHelper.createRelatedWork(relatedWorkMap, issueKeyToWorkIdMap);

        console.log(`Skipped ${numIssuesSkipped} Jira issues during migration.`);
    } catch (ex) {
        console.error(ex);
    }
})();
